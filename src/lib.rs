// SPDX-License-Identifier: Apache-2.0 OR MIT

//! # Aid
//!
//! ## What is Aid?
//! Aid is an ID generation algorithm used by [Misskey](https://github.com/misskey-dev/misskey). It’s short and has millisecond accuracy.
//!
//! ```text
//!     8uj736jj              5p
//! |-  Timestamp  -|  |- Randomness -|
//!   8 characters       2 characters
//! ```

use chrono::prelude::*;
use chrono::{Duration, Utc};
use lazy_static::lazy_static;
use lexical::{to_string_with_options, NumberFormatBuilder, WriteIntegerOptions};
use thiserror::Error;

lazy_static! {
    static ref AID_EPOCH: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();
}

const FORMAT: u128 = NumberFormatBuilder::new().radix(36).build();

/// Aid error
#[derive(Error, Debug)]
pub enum DecodeAidError {
    #[error("{} is invaild AID. It's too large.", .0)]
    TooLarge(String),

    #[error("{} is invaild AID. It's too short.", .0)]
    TooShort(String),

    #[error("{} is invaild AID. (at character {})", .1, .0)]
    Invaild(usize, String),

    #[error("Timestamp overflowed.")]
    OverflowedTimestamp,
}

/// An Aid.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
pub struct Aid {
    /// timestamp
    pub time: DateTime<Utc>,
    noise: u16,
}

impl PartialOrd for Aid {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.time.partial_cmp(&other.time)
    }

    fn lt(&self, other: &Self) -> bool {
        self.time.lt(&other.time)
    }

    fn le(&self, other: &Self) -> bool {
        self.time.le(&other.time)
    }

    fn gt(&self, other: &Self) -> bool {
        self.time.gt(&other.time)
    }

    fn ge(&self, other: &Self) -> bool {
        self.time.ge(&other.time)
    }
}

impl Ord for Aid {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.time.cmp(&other.time)
    }
}

impl TryFrom<String> for Aid {
    type Error = anyhow::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Aid::decode(value)
    }
}

impl TryFrom<&str> for Aid {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Aid::decode(value)
    }
}

impl ToString for Aid {
    fn to_string(&self) -> String {
        self.encode()
    }
}

impl Aid {
    /// Generate Aid.
    pub fn generate() -> Aid {
        use rand::prelude::*;

        let mut rng = rand::thread_rng();
        let n = rng.gen_range(0..1295); // 0 to ZZ in base 36.

        Aid {
            time: Utc::now(),
            noise: n,
        }
    }

    /// Encode Aid
    fn encode(&self) -> String {
        let t = self
            .time
            .signed_duration_since(*AID_EPOCH)
            .num_milliseconds();

        let options = WriteIntegerOptions::default();

        let t = to_string_with_options::<_, FORMAT>(t, &options);
        let n = to_string_with_options::<_, FORMAT>(self.noise, &options);

        format!("{:0>8}{:0>2}", t, n)
    }

    /// Decode Aid
    fn decode<S: AsRef<str>> (input: S) -> anyhow::Result<Aid> {
        let input = input.as_ref();
        for (i, c) in input.chars().enumerate() {
            if !c.is_ascii_alphanumeric() {
                return Err(DecodeAidError::Invaild(i, input.to_string()).into());
            }
        }

        let (t, n) = match (input.len() > 10, input.len() < 10) {
            (true, _) => Err(DecodeAidError::TooLarge(input.to_string())),
            (_, true) => Err(DecodeAidError::TooShort(input.to_string())),
            (false, false) => Ok(input.split_at(8)),
        }?;

        let t = i64::from_str_radix(t, 36)?;
        let n = u16::from_str_radix(n, 36)?;

        let t = AID_EPOCH
            .checked_add_signed(Duration::milliseconds(t))
            .ok_or(DecodeAidError::OverflowedTimestamp)?;

        Ok(Aid { time: t, noise: n })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode() {
        let aid = Aid {
            time: Utc.with_ymd_and_hms(2002, 7, 31, 0, 0, 0).unwrap(),
            noise: 0,
        };

        assert_eq!("11E0SG0000", aid.encode());
    }

    #[test]
    fn test_decode() {
        let aid = Aid {
            time: Utc.with_ymd_and_hms(2002, 7, 31, 0, 0, 0).unwrap(),
            noise: 0,
        };

        let decoded_aid = Aid::decode("11E0SG0000").unwrap();

        assert_eq!(aid, decoded_aid);
    }

    #[test]
    #[should_panic(expected = "11E0SG00000 is invaild AID. It's too large.")]
    fn test_fail_decode_too_large() {
        let _ = Aid::decode("11E0SG00000").unwrap();
    }

    #[test]
    #[should_panic(expected = "11E0SG000 is invaild AID. It's too short.")]
    fn test_fail_decode_too_short() {
        let _ = Aid::decode("11E0SG000").unwrap();
    }

    #[test]
    #[should_panic(expected = "えーあいでー is invaild AID. (at character 0)")]
    fn test_fail_decode_invaild() {
        let _ = Aid::decode("えーあいでー").unwrap();
    }
}
