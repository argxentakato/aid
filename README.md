# aid &emsp; [![Build Status]][pinelines] [![Latest Version]][crates.io]

**A Rust port of the Aid. It's an ID generation algorithm used in [Misskey](https://github.com/misskey-dev/misskey).**

[Build Status]: https://img.shields.io/gitlab/pipeline/silverscat_3/aid?style=flat-square
[pinelines]: https://gitlab.com/silverscat_3/aid/-/pipelines
[Latest Version]: https://img.shields.io/crates/v/aid.svg?style=flat-square
[crates.io]: https://crates.io/crates/aid

---

## License 

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](./LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](./LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.

## Author

銀猫さん <silverscat_3@mail.sc3.fun>

