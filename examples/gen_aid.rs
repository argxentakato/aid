use aid::Aid;

fn main() {
    let aid = Aid::generate().to_string();
    println!("{}", aid);

    let aid = Aid::try_from("8uko7hlhfm").unwrap();
    println!("\"8uko7hlhfm\" is generated at {}", aid.time);
}
